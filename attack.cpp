#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main(){
  string message1 = "attack at dawn";
  int message1_in_hex[message1.size()];
  for(int i=0; i<message1.size(); i++)
    message1_in_hex[i] = (int)message1[i];
  int ciphertext_in_hex[] = { 0x09, 0xe1, 0xc5,0xf7, 0x0a, 0x65, 0xac, 0x51, 0x94, 0x58, 0xe7, 0xe5, 0x3f, 0x36};
  
  int key_in_hex[message1.size()];
  for(int i=0; i<message1.size(); i++){
    key_in_hex[i] = message1_in_hex[i]^ciphertext_in_hex[i];
    cout<<key_in_hex[i]<<" ";
  }
  cout<<endl;
  string message2 = "attack at dusk";
  int message2_in_hex[message2.size()];
  int ciphertex2[message2.size()];
  for(int i=0; i<message1.size(); i++){
    message2_in_hex[i] = (int)message2[i];
    ciphertex2[i] = message2_in_hex[i]^key_in_hex[i];
    cout<<hex<<ciphertex2[i];
  }
  cout<<endl;
}
