#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

typedef int* HexArray;

int decimal(int number){
  return number;
}

class HexStringToHexArray{
  public:
    HexArray hexArray;
    int stringLength;
    HexStringToHexArray(string hexString);
    HexStringToHexArray(HexArray array, int length);
    HexArray getHexArray();
    int getStringLength();
    void printHexString();
    static void printHexStringToFile(fstream *myfile, HexStringToHexArray **hexStringArray, int count, HexStringToHexArray *hexString2);
    HexStringToHexArray* operator^(HexStringToHexArray* hexString);
    ~HexStringToHexArray();
};

HexStringToHexArray::~HexStringToHexArray(){
  delete[] hexArray;
}

int HexStringToHexArray::getStringLength(){
  return stringLength;
}

HexStringToHexArray::HexStringToHexArray(HexArray array, int length){
  stringLength = length;
  hexArray = new int[length];
  for(int i=0; i<length; i++){
    hexArray[i] = array[i];
  }
}

HexStringToHexArray::HexStringToHexArray(string hexString){
  stringLength = hexString.size()/2;
  hexArray = new int[hexString.size()/2];
  for(int i=0; i<(int)hexString.size(); i+=2){
    stringstream ss1, ss2;
    ss1.put(hexString[i]);
    int number, number1, number2;
    ss1>>number1;
    if(ss1.fail()){
      if((int)hexString[i]>=97 && (int)hexString[i]<103){
        number1 = (int)hexString[i]-87;
      }
    }
    ss2.put(hexString[i+1]);
    ss2>>number2;
    if(ss2.fail()){
      if((int)hexString[i+1]>=97 && (int)hexString[i+1]<103){
        number2 = (int)hexString[i+1]-87;
      }
    }
    //convert the number to decimal before putting it in the array;
    number = number1*16+number2;
    hexArray[i/2] = number;
  }
}

HexArray HexStringToHexArray::getHexArray(){
  return hexArray;
}

void HexStringToHexArray::printHexStringToFile(fstream *myfile, HexStringToHexArray **hexStringArray, int cipherNum, HexStringToHexArray *targetCipher){
  //again its a bit messy since hexString1 and hexString2 will both have different lengths.. 
  //What we can do is just print out a file that will have only the lengths of the shortest length of text available
#if 0
  (*myfile)<<"Xor CipherText1 + Ciphertext2, Xor Ciphertext1 + Ciphertext3\n";
  int stringLength1 = hexString1->getStringLength();
  int stringLength2 = hexString2->getStringLength();
  int length = (stringLength1 < stringLength2) ? stringLength1 : stringLength2;
  HexArray hArray1 = hexString1->getHexArray();
  HexArray hArray2 = hexString2->getHexArray();
  for(int i=0; i<length; i++){
    (*myfile)<<hArray1[i]<<","<<hArray2[i]<<endl;
  }
#endif

  (*myfile)<<"Target CipherText, ";
  for(int i=0; i< cipherNum-1; i++){
    (*myfile)<<"CipherText "<< i+1 <<",";
  }
  (*myfile)<<"CipherText "<<cipherNum<<"\n";
  //first header line is written into the file..
  //now the rest of the xor'd outputs;

  int lengthsOfStrings[cipherNum];
  for(int i=0; i<cipherNum; i++){
    lengthsOfStrings[i] = hexStringArray[i]->getStringLength();
  }
  
  int minimumLength = lengthsOfStrings[0];
  for(int i=0; i<cipherNum; i++){
    if(minimumLength > lengthsOfStrings[i]){
      minimumLength = lengthsOfStrings[i];
    }
  }

  //found the minimum length for the all the xored inputs;
  for(int i=0; i<minimumLength; i++){
    (*myfile)<<targetCipher->hexArray[i]<<", ";
    for(int j=0; j<cipherNum-1; j++){
       (*myfile)<<hexStringArray[j]->hexArray[i]<<", ";
    }
    (*myfile)<<hexStringArray[cipherNum-1]->hexArray[i]<<"\n";
  }
}

void HexStringToHexArray::printHexString(){
  for(int i=0; i<stringLength; i++){
    cout << hexArray[i] << " ";
  }
  cout<<endl;

  cout<<"The length of the string is : "<< dec << stringLength <<endl;
}

HexStringToHexArray* HexStringToHexArray::operator^(HexStringToHexArray *hexString){
  HexArray passedHexArray = hexString->getHexArray();
  
  int passedStringLength = hexString->getStringLength();
  int length;
  if(passedStringLength <= stringLength){
    length = passedStringLength;
  }else{
    length = stringLength;
  }

  HexArray hexArrayTemp = new int[length];

  for(int i=0; i<length; i++){
    hexArrayTemp[i] = hexArray[i]^(passedHexArray[i]);
  }

  //should I not return a new object here.. 
  //it will be better since we don't want to tamper with the current object..
  HexStringToHexArray *TempObject  = new HexStringToHexArray(hexArrayTemp, length);
  delete[] hexArrayTemp;
  return TempObject;
}

int main(int argc, char** argv){
  fstream myfile;
#if 0
  cout<<"Please enter the ciphertext hex string: ";
  string stringInput1, stringInput2, stringInput3;
  cin>>stringInput1;
  cout<<"Please enter the second ciphertext hex string: ";
  cin>>stringInput2;
  cout<<"Please enter the third ciphertext hex string: ";
  cin>>stringInput3;
  HexStringToHexArray *hexString1 = new HexStringToHexArray(stringInput1);
  HexStringToHexArray *hexString2 = new HexStringToHexArray(stringInput2);
  HexStringToHexArray *hexString3 = new HexStringToHexArray(stringInput3);
  HexStringToHexArray *xorStringOutput1 = (*hexString1)^(hexString2);
  HexStringToHexArray *xorStringOutput2 = (*hexString1)^(hexString3);
  myfile.open("output.csv", ios::out);
  HexStringToHexArray::printHexStringToFile(&myfile, xorStringOutput1, xorStringOutput2);
  myfile.close();
  cout<<endl<<"The output has been written to the csv file output.csv"<<endl;
#endif
  //Lets do this in a loop accepting as many strings as possible.
  //We might need to have enough xorStringOutput objects to store and print out our file
  
  cout<<"Please enter the Target CipherText: ";
  string targetCipher;
  cin>>targetCipher;
  HexStringToHexArray *targetString = new HexStringToHexArray(targetCipher);
  int cipherNum = 0;
  HexStringToHexArray **xorStringOutputArray;
  xorStringOutputArray = new HexStringToHexArray*[100];
  string reply;
  do{
    cout<<"Enter the ciphertext number "<<cipherNum+1<<": ";
    string stringInput;
    cin>>stringInput;
    HexStringToHexArray hexString(stringInput);
    
    xorStringOutputArray[cipherNum++] = (*targetString)^(&hexString);
    cout<<endl<<"Do you want to input another ciphertext [y/Y/n/N]: ";
    cin>>reply;
  }while((reply.compare("y") == 0) || (reply.compare("Y") == 0));

#if 0
  delete hexString1;
  delete hexString2;
  delete hexString3;
  delete xorStringOutput1;
  delete xorStringOutput2;
#endif

  myfile.open("output.csv", ios::out);
  HexStringToHexArray::printHexStringToFile(&myfile, xorStringOutputArray, cipherNum, targetString);
  myfile.close();

  delete[] xorStringOutputArray;
  delete targetString;
}
