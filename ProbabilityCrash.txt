Random Variable:

A random variable is a function X:U-->V. Example X:{0,1}^n --> {0,1}; X(y) = lsb(y) 

For the uniform distribution on U:

Pr[X=1] = 0.5 and Pr[X=0] = 0.5

Random Variable is basically a function then.. and thus induces a distribution on V: could be one element, could be a set.


Uniform Random Variable: Identity functions are uniform random variables.. Pr[r = a] = 1/|U|. Formally r(x) = x;

Randomized Algorithm:

y <== A(m,r) where r <-- {0,1}^n 


Independence:

Def: events A and B are independent if Pr[A and B] = Pr[A] * Pr[B]

Random variable X and Y are independent if Pr[X=a and X=b] = Pr[X=a] * Pr[Y=b]

Birthday Paradox:

Given r1, ... , rn belonging to U be independent identically distributed random variable :

Theorem: when n=1.2*U^0.5 then the Pr[i!=j; ri=rj] >= 1/2

notation: |U| is the size of U

Example: Let U = {0,1}^128

After sampling about 2^64 random messages from U, some two sampled messages will likely be the same.
